#include "Message.h"
#include "AbstractNetworkUnit.h"

#ifndef ICPP_TEAMBUILDING_MESSAGEPORT_H
#define ICPP_TEAMBUILDING_MESSAGEPORT_H


class AbstractNetworkUnit;

class MessagePort {
public:
    MessagePort(Message *message, AbstractNetworkUnit *abstractNetworkUnit);

    MessagePort();

private:
    Message* message;
    AbstractNetworkUnit* abstractNetworkUnit;
};

MessagePort::MessagePort() {}

MessagePort::MessagePort(Message *message, AbstractNetworkUnit *abstractNetworkUnit) : message(message),
                                                                                       abstractNetworkUnit(
                                                                                               abstractNetworkUnit) {}

#endif //ICPP_TEAMBUILDING_MESSAGEPORT_H
