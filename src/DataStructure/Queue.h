//
// Created by david on 02.12.2019.
//

#ifndef ICPP_TEAMBUILDING_QUEUE_H
#define ICPP_TEAMBUILDING_QUEUE_H

template<typename T>
struct Queue {
private:
    struct El {
        T value;
        El *next;
        El *previous;
    };

    El *first;
    El *last;
public:
    Queue() {
        first = last = nullptr;
    }

    Queue(const Queue &f) {
        El *it = f.prvni;
        while (it) {
            Insert(it->value);
            it = it->next;
        }
    }

    ~Queue() {
        while (first) {
            El *tmp = first;
            first = first->next;
            delete tmp;
        }
    }

    void Insert(T value) {
        last = new El{value, nullptr, last};
        if (!first)
            first = last;
        else
            last->previous->next = last;
    }

    T Remove() {
        T value = last->value;
        El *tmp = last;

        if (last->previous) {
            last = last->previous;
            last->next = nullptr;
        } else {
            last = nullptr;
            first = nullptr;
        }

        delete tmp;
        return value;
    }

    bool Contains(T value) const {
        El *el = first;
        while (el) {
            if (el->value == value)
                return true;
            el = el->next;
        }
        return false;
    }

    using ApplyFunkce = void (*)(T);

    void ProcessElement(ApplyFunkce f) {
        El *el = first;
        while (el) {
            f(el->value);
            el = el->next;
        }
    }

    bool IsEmpty() const {
        return first == nullptr;
    }
};


#endif //ICPP_TEAMBUILDING_QUEUE_H
