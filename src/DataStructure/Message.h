//
// Created by david on 02.12.2019.
//

#ifndef ICPP_TEAMBUILDING_MESSAGE_H
#define ICPP_TEAMBUILDING_MESSAGE_H

#include<iostream>

using namespace std;

struct Message {
    int id;
    string sourceAddress;
    string targetAddress;
    string content;

    Message() {}

    Message(int id, const string &sourceAddress, const string &targetAddress, const string &content) : id(id), sourceAddress(sourceAddress), targetAddress(targetAddress), content(content) {}
};


#endif //ICPP_TEAMBUILDING_MESSAGE_H
