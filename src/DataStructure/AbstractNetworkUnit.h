//
// Created by david on 02.12.2019.
//

#ifndef ICPP_TEAMBUILDING_ABSTRACTNETWORKUNIT_H
#define ICPP_TEAMBUILDING_ABSTRACTNETWORKUNIT_H


#include "MessagePort.h"
#include "Queue.h"

class AbstractNetworkUnit {
protected:
public:
    AbstractNetworkUnit();

protected:
    Queue<MessagePort> queue;

    virtual void ProcessIncomingMessage(MessagePort messagePort) = 0;

public:
    virtual void Flow() = 0;
    virtual void Connect(AbstractNetworkUnit* networkUnit) = 0;
};


#endif //ICPP_TEAMBUILDING_ABSTRACTNETWORKUNIT_H
