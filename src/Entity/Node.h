//
// Created by david on 02.12.2019.
//

#ifndef ICPP_TEAMBUILDING_NODE_H
#define ICPP_TEAMBUILDING_NODE_H


class Node : AbstractNetworkUnit {
private:
    string address;
    AbstractNetworkUnit* connectedNetworkUnit;
    Queue<Message*> outgoingMessages;
public:
    Message PrepareMessageForSend(string target, string content);

private:
    void ProcessIncomingMessage(MessagePort messagePort) override;

    void Flow() override;

    void Connect(AbstractNetworkUnit* networkUnit) override;
};


#endif //ICPP_TEAMBUILDING_NODE_H
