//
// Created by david on 02.12.2019.
//

#ifndef ICPP_TEAMBUILDING_HUB_H
#define ICPP_TEAMBUILDING_HUB_H


#include "../DataStructure/AbstractNetworkUnit.h"
#include "../DataStructure/Queue.h"
#include "../DataStructure/Message.h"
#include "../DataStructure/MessagePort.h"

class Hub : AbstractNetworkUnit {
public:
    Hub(int maxConnections);

    void Connect(AbstractNetworkUnit* networkUnit);

    void ProcessIncomingMessage(MessagePort messagePort);

private:
    void Flow() override;

private:
    AbstractNetworkUnit** connectedUnits;
    int maximumAllowedConnections;
    Queue<Message*> processedMessages;
    int numberOfConnectedUnits;
};


#endif //ICPP_TEAMBUILDING_HUB_H
