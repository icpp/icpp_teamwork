//
// Created by david on 02.12.2019.
//

#include "Hub.h"

Hub::Hub(int maxConnections) {
    this->connectedUnits = new AbstractNetworkUnit *[maxConnections];
    this->numberOfConnectedUnits = 0;
    this->maximumAllowedConnections = maxConnections;
}

void Hub::Connect(AbstractNetworkUnit* networkUnit) {
    if (this->maximumAllowedConnections > this->numberOfConnectedUnits) {
        this->connectedUnits[this->numberOfConnectedUnits++] = networkUnit;
    } else {
        throw overflow_error("Number of connected devices exceed limit!");
    }
}

void Hub::ProcessIncomingMessage(MessagePort messagePort) {

}

void Hub::Flow() {

}
